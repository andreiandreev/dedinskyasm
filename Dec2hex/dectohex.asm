; nasm -f elf64 -l 1-nasm.lst 1-nasm.s  ;  ld -s -o 1-nasm 1-nasm.o

section .text

global dectohex

dectohex:  
pop rbx     
mov dl, 16d
push 0x2

next:
       
        div dl
        mov cl, al
        cmp ah, 10d
        jae  addmore
        jl addless     

back:
        mov al, 0
        xchg ah, al
        push ax
        
        mov ah, 0
        mov al, cl
        cmp al, 0
        jne next

write:
        
        mov rax, 0x01
        mov rdi, 1
        mov rsi, rsp
        mov rdx, 1
        syscall
        pop cx
        cmp cx, 0x2
        jne write
        push rbx
ret

addmore:
        add ah, 'A' - 10
        jmp back

addless:
        add ah, '0'        
        jmp back

global _start

_start:     

        mov ax, 324d    

        call dectohex

        mov rax, 0x3c
        xor rdi, rdi
        syscall 