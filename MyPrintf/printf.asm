; nasm -f elf64 -l 1-nasm.lst 1-nasm.s  ;  ld -s -o 1-nasm 1-nasm.o
%include "printBin.asm"
%include "printChar.asm"
%include "printDec.asm"
%include "printHex.asm"
%include "printString.asm"


section .text
global myPrintf
global _start

myPrintf:
; Ruins ax, bx, es, dx, cx
; Push parameters, then push str adress
        
        pop rbx 
        push r9
        push r8     
        push rcx                
        push rdx
        push rsi
        push rdi
        pop rdx
        push rdx
        mov r12, rbx
        call strlen
        mov rbx, r12
        mov rdi, rdx

repeat:
        cmp cx, 0xffff
        je repeatexit
comp:
        cmp byte [rdi], '%'
        jne printsymbol
        dec cx
        inc rdi
        cmp byte [rdi], 'c'
        je printc
        cmp byte [rdi], 'b'
        je printb
        cmp byte [rdi], 'd'
        je printd
        cmp byte [rdi], 'h'
        je printh
        cmp byte [rdi], '%'
        je printperc
        cmp byte [rdi], 's'
        je prints
printperc:
        push '%'
printc:
        mov r12, rbx
        mov r13, rcx
        mov r14, rdi
        mov r15, rsp
        mov rdi, rsp
        call printChar
        mov rbx, r12
        mov rcx, r13
        mov rsp, r15
        pop rdi
        mov rdi, r14
        jmp printend

printsymbol:
        mov r12, rbx
        mov r13, rcx
        mov r14, rdi
        mov r15, rdx
        call printChar
        mov rbx, r12
        mov rcx, r13
        mov rdi, r14
        mov rdx, r15

        jmp printend
prints:
        mov r12, rbx
        mov r13, rcx
        mov r14, rdi
        mov r15, rdx
        call printString
        mov rbx, r12
        mov rcx, r13
        mov rdi, r14
        mov rdx, r15
        jmp printend
printb:
        pop rax
        mov r12, rbx
        mov r13, rcx
        mov r14, rdi
        mov r15, rdx
        call printBin
        mov rbx, r12
        mov rcx, r13
        mov rdi, r14
        mov rdx, r15
        jmp printend
printd:
        pop rax
        mov r12, rbx
        mov r13, rcx
        mov r14, rdi
        mov r15, rdx
        call printDec
        mov rbx, r12
        mov rcx, r13
        mov rdi, r14
        mov rdx, r15
        jmp printend

printh:
        pop rax
        mov r12, rbx
        mov r13, rcx
        mov r14, rdi
        mov r15, rdx
        call printHex
        mov rbx, r12
        mov rcx, r13
        mov rdi, r14
        mov rdx, r15
        jmp printend
printend:
        inc rdi
        dec cx
        cmp cx, 0
        jne repeat
repeatexit:
        mov rsp, rbp
        push rbx
ret
    
