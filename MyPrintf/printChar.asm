; nasm -f elf64 -l 1-nasm.lst 1-nasm.s  ;  ld -s -o 1-nasm 1-nasm.o

section .text

global printChar
global _start

printChar:
            
            pop rbx
            mov rax, 0x01
            mov rsi, rdi
            mov rdi, 1
            mov rdx, 1
            push rbx
            syscall
            ret
