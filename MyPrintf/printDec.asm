
; nasm -f elf64 -l 1-nasm.lst 1-nasm.s  ;  ld -s -o 1-nasm 1-nasm.o

section .text

global printDec
global _start

printDec:
        pop rbx
        mov dl, 10d
        push 0x2

nextDec:
       
        div dl
        mov cl, al
        add ah, 48d     ; reminder to ascii
        mov al, 0
        xchg ah, al
        push rax
        
        mov ah, 0
        mov al, cl
        cmp al, 0
        jne nextDec
            
writeDec:
        
        mov rax, 0x01
        mov rdi, 1
        mov rsi, rsp
        mov rdx, 1
        syscall
        pop rcx
        cmp cx, 0x2
        jne writeDec
        push rbx   
ret
