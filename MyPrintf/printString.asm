 ; nasm -f elf64 -l 1-nasm.lst 1-nasm.s  ;  ld -s -o 1-nasm 1-nasm.o
%include "strlen.asm" 
section .text

global printString


printString:

        pop rbx
        pop rax
        push rbx
        push rax
        call strlen
        
        mov rax, 0x01
        mov rdi, 1
        mov rdx, rcx
        syscall
ret
    
